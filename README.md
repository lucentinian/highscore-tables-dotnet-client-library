# highscore-tables-dotnet-client-library

# Highscore tables / Leaderboards .NET client library

Use this library to enable your .NET projects to access your Highscore tables / Leaderboards created in our [Highscores Tables API webservice](https://highscores.ehehdada.com)

Add it with NuGet to your projects!